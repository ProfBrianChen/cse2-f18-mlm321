//Martin Magazzolo
//9/18/18
//CSE02
//This program will determine the rainfall in an inputted area
import java.util.Scanner;
public class Convert {
    public static void main(String[] args) {
      
      Scanner myScanner = new Scanner (System.in); //Making a scanner for the user to input the necessary data
      
      System.out.print("Enter the affected area in acres:"); //Asking for the area of the rainfall
      double Area = myScanner.nextDouble();
      
      System.out.print("Enter the rainfall in the affected area:"); //Asking for the rainfall in that area in inches
      double Rainfall = myScanner.nextDouble();
      
      Area = Area * 6273000; //Converting the area in acres to square inches
      double TotalRain = Area * Rainfall; //Finding the total rainfall
      
      TotalRain = TotalRain * (3.93 * Math.pow(10,-15)); //Converting the rain from cubic inches to cubic miles
      
      System.out.println(TotalRain + " cubic miles"); //Displaying the total rainfall in cubic miles
      
    }
}
      
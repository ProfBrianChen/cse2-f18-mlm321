//Martin Magazzolo
//9/18/18
//CSE02
//This program will determine the volume inside of a pyramid
import java.util.Scanner;
public class Pyramid {
    public static void main(String[] args) {
       
      Scanner myScanner = new Scanner (System.in); //Creating a scanner for inputting data
      
      System.out.print("What is the length of the square side of the pyramid? ");
      double Square = myScanner.nextDouble();
      System.out.print("What is the height of the pyramid? ");
      double Height = myScanner.nextDouble();
      
      //Prompting the user to input the data of the pyramid and storing the data to be used in calculations
      
      double Volume = (Square * Square * Height) / 3; //Computing the volume of the pyramid with the formula V=(l*w*h)/3
      
      System.out.println("The volume of the pyramid is: " + Volume);
      //Printing the result for the volume of the pyramid
    }
}
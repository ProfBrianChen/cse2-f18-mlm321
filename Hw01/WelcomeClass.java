public class WelcomeClass{
public static void main(String args[]){
System.out.println("   -----------");
System.out.println("   | WELCOME |");
System.out.println("   -----------");
System.out.println("   ^  ^  ^  ^  ^  ^");
System.out.println("  / \\/ \\/ \\/ \\/ \\/ \\"); // Last 5 lines are printing the welcome message
System.out.println(" <-M--L--M--3--2--1->"); //Printing my Lehigh Network ID
System.out.println("  \\ /\\ /\\ /\\ /\\ /\\ /");
System.out.println("   v  v  v  v  v  v"); // Last two lines printing out the rest of the welcome message
System.out.println("My name is Martin Magazzolo, but most people call me Tino as a nickname.  I am a sophomore and I am studying Chemical Engineering, but I also have a big interest in Computer Science. I live a little bit outside Philadelphia, and I really love Philly sports, mostly the Sixers and Eagles. Another fact about me at Lehigh is that I am part of the Ultimate Frisbee team.  I just started playing last year but it is already one of my favorite things to do now. Also, I like playing video games on my Xbox when I'm not at class, frisbee, or doing homework. "); //Printing my biographical statement
}
}

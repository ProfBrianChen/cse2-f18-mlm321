//Martin Magazzolo
//11/13/18
//CSE02
//This program will simulate shuffling a deck of cards
import java.util.Scanner;
import java.util.Random;
public class hw08 {
  public static String[] shuffle(String input[]){
    for(int x =0; x<1000; x++){ //looping 1000 times so it is definitely shuffled
      Random randomGenerator =  new Random(); //creating a random number generator
      int Random = randomGenerator.nextInt(52); //generating a number from 1 to 52
      String old = input[0]; //storing the first number to swap it
      input[0] = input[Random]; //reassigning the first spot to the card from the random spot
      input[Random] = old; //Putting the old first card to the random spot
    }
  return input; //returning the array
  }
 public static String[] getHand(String input[], int index, int numCards){
 String hand[] = new String[numCards]; //Creating a new array for the hand
   for(int x = 0; x < numCards; x++){ //Creating a for loop that goes from 0 to the number of cards wanted in the array
   hand[x] = input[index - x]; //assiging the card to the last cards from the original array
 }
   return hand; //returning the array
 }
  public static void printArray(String input[]){
  int size = input.length;
    for(int x= 0; x <size; x++){ //Creating a for loop for each card in the array
    System.out.print(input[x] + " ");  //Printing each card 
  }
    return;
  }
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; 
String[] hand = new String[5]; 
int numCards = 5; 
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  //System.out.print(cards[i]+" "); 
} 
System.out.println();
printArray(cards); 
System.out.println();
String[] shuffled = shuffle(cards); 
System.out.println("Shuffled");
printArray(shuffled); 
while(again == 1){ 
   if (index < 5 ) {
     break;
   }
   hand = getHand(cards,index,numCards); 
   System.out.println();
   printArray(hand);
   index -= numCards;
   System.out.println("Enter a 1 if you want another hand drawn"); 
   again = scan.nextInt(); 
}
  } 
}
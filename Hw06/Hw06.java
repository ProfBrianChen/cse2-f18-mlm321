//Martin Magazzolo
//10/23/18
//CSE02
//This program will create an encrypted X based on a user input
import java.util.Scanner;
public class Hw06{
    public static void main(String[] args) {
      Scanner myScanner = new Scanner (System.in); //Creating a scanner
      int input = 0;
      double rows = 0;
       do {
        System.out.println("Enter an integer between 1 and 10.");
        if (myScanner.hasNextInt()== true){ //Determining if the user inputted an integer
           input = myScanner.nextInt(); //Assigning the input to an integer
        }
        else {
          System.out.println("Error: Integer input required"); //If an integer wasnt inputted, an error message is displayed
          myScanner.next();
        }
      }
      while ( input > 10 || input < 1); //Do while loop that verifies a correct input
      
      for (int x = 0; x <=input; x++){ // Creating a loop that loops for each row
        for(int y = 0; y<=input; y++){ // creating a loop that loops for each input in each row
          if (y == x) {
            System.out.print(" "); // If y = x that means the spot is on the diagonal, so it is assigned a blank space
          }
          else if(y == (input - x)){
            System.out.print(" "); // if y = rows - x, that means that the spot is on the backwards diagonal, so it also gets a blank space
          }
          else{
            System.out.print("*"); // Every other spot gets an asterix
          }
        }
        System.out.println(""); // Creating a new line
      }
    }
}
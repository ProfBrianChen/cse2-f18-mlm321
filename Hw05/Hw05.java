//Martin Magazzolo
//10/9/18
//CSE02
//This program will simulate a game of poker
import java.util.Scanner;
public class Hw05 {
    public static void main(String[] args) {
      int x = 0;
      int FourKind = 0;
      int ThreeKind = 0;
      int TwoPair = 0;
      int OnePair = 0;
      int Hands = 0;
      
      //Setting up counters
      
      Scanner myScanner = new Scanner (System.in); //Creating a scanner
      System.out.println("How many hands should be generated?"); //Letting the user choose how many hands should be generated
      if (myScanner.hasNextInt() == true) { //Determining if the user inputted an integer
          Hands = myScanner.nextInt(); //Assigning the input to an integer
        }
      else {
          System.out.println("Error: Integer input required"); //If an integer wasnt inputted, an error message is displayed
          myScanner.next(); // Resetting the scanner
        }
     
      while (x < Hands) {
        int y = 0;
        int number1 = 0;
        int number2 = 0;
        int number3 = 0;
        int number4 = 0;
        int number5 = 0;
        int number6 = 0;
        int number7 = 0;
        int number8 = 0;
        int number9 = 0;
        int number10 = 0;
        int number11 = 0;
        int number12 = 0;
        int number13 = 0;
        int Card1 = -1;
        int Card2 = -1;
        int Card3 = -1;
        int Card4 = -1;
        int Card;
       //Creating counters to determine how many of each number is generated in the hand
        while (y < 5){
        do{
         Card = (int)(Math.random() * 51); // Generating a number from 0 to 1 then scaling it to a number between 0 and 51
        } while(Card == Card1|| Card == Card2 || Card == Card3|| Card == Card4 );
           switch(y) {
             case 0:
               Card1 = Card;
             break;
             case 1:
               Card2 = Card;
             break;
             case 2:
               Card3 = Card;
             break;
             case 3:
               Card4 = Card;
             break;
           }
        int remainder = Card % 13; //Creating groups of 13 to be able to assign the card number based on the remainder
            switch( remainder) {
              case 0:
                number1++;
              break;
              case 1:
                number2++;
              break;
              case 2:
                number3++;
              break;
              case 3:
                number4++;
              break;
              case 4:
                number5++;
              break;
              case 5:
                 number6++;
              break;
              case 6:
                 number7++;
              break;
              case 7:
                 number8++;
              break;
              case 8:
                number9++;
              break;
              case 9:
                number10++;
              break;
              case 10:
                number11++;
              break;
              case 11:
                number12++;
              break;
              case 12:
                number13++;
              break;
              } 
             //This switch statement analyzes each number generated and determines what number it is
        
             y++; //Incrementing the counter for the loop
         }
        if(number1==4||number2==4||number3==4||number4==4||number5==4||number6==4||number7==4||number8==4||number9==4||number10==4||number11==4||number12==4||number13==4) {
           FourKind++;
          }
          //If any of the numbers appear 4 times, then the counter for four of a kind gets incremented
          if(number1==3||number2==3||number3==3||number4==3||number5==3||number6==3||number7==3||number8==3||number9==3||number10==3||number11==3||number12==3||number13==3) {
           ThreeKind++;
            }
          //If any of the numbers appear 3 times, then the counter for three of a kind gets incremented
          if(number1==2){
            if(number2==2||number3==2||number4==2||number5==2||number6==2||number7==2||number8==2||number9==2||number10==2||number11==2||number12==2||number13==2){
              TwoPair++;
            }
             else{
               OnePair++;
             } 
            }
          //checking to see if any of the numbers appear twice. If it appears twice, it checks to see if another number appears twice as well. If so, two pair gets incremented. If not, one pair gets incremented.
          if(number2==2){
            if(number3==2||number4==2||number5==2||number6==2||number7==2||number8==2||number9==2||number10==2||number11==2||number12==2||number13==2){
              TwoPair++;
            }
             else{
               OnePair++;
             }
            }
          if(number3==2){
            if(number4==2||number5==2||number6==2||number7==2||number8==2||number9==2||number10==2||number11==2||number12==2||number13==2){
              TwoPair++;
            }
             else{
               OnePair++;
             }
            }
          if(number4==2){
            if(number5==2||number6==2||number7==2||number8==2||number9==2||number10==2||number11==2||number12==2||number13==2){
              TwoPair++;
            }
             else{
               OnePair++;
             }
            }
          if(number5==2){
            if(number6==2||number7==2||number8==2||number9==2||number10==2||number11==2||number12==2||number13==2){
              TwoPair++;
            }
             else{
               OnePair++;
             }
            }
          if(number6==2){
            if(number7==2||number8==2||number9==2||number10==2||number11==2||number12==2||number13==2){
              TwoPair++;
            }
             else{
               OnePair++;
             }
            }
          if(number7==2){
            if(number8==2||number9==2||number10==2||number11==2||number12==2||number13==2){
              TwoPair++;
            }
             else{
               OnePair++;
             }
            }
          if(number8==2){
            if(number9==2||number10==2||number11==2||number12==2||number13==2){
              TwoPair++;
            }
             else{
               OnePair++;
             }
            }
          if(number9==2){
            if(number10==2||number11==2||number12==2||number13==2){
              TwoPair++;
            }
             else{
               OnePair++;
             }
            }
          if(number10==2){
            if(number11==2||number12==2||number13==2){
              TwoPair++;
            }
             else{
               OnePair++;
             }
            }
          if(number11==2){
            if(number12==2||number13==2){
              TwoPair++;
            }
             else{
               OnePair++;
             }
            }
          if(number12==2){
            if(number13==2){
              TwoPair++;
            }
             else{
               OnePair++;
             }
            }
          if(number13==2){
            OnePair++;
          }
        x++;
      }
      
      OnePair=OnePair - TwoPair;
      //Taking out all of the cases where a two pair is counted as a one pair
      double ProbabilityFour = ((double) FourKind) / Hands ; 
      double ProbabilityThree = ((double) ThreeKind) / Hands ;
      double ProbabilityTwo = ((double) TwoPair) / Hands ;
      double ProbabilityOne = ((double) OnePair) / Hands ;
      //Determining the probabilities of all the possibilities
      System.out.println(FourKind);
      System.out.println(ThreeKind);
      System.out.println(TwoPair);
      System.out.println(OnePair);
      System.out.println("The number of Hands: " + x);
      System.out.printf("Probability of Four of a Kind: %4.3f\n", ProbabilityFour);
      System.out.printf("Probability of Three of a Kind: %4.3f\n", ProbabilityThree);
      System.out.printf("Probability of Two Pairs: %4.3f\n", ProbabilityTwo);
      System.out.printf("Probability of One Pair: %4.3f\n", ProbabilityOne);
}
}
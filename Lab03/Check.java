//Martin Magazzolo
//9/13/18
//CSE02
//This program is going to calculate how to split a check evenly from the inputted price and tip
import java.util.Scanner;
public class Check {
    public static void main(String[] args) {
      
      Scanner myScanner = new Scanner (System.in); //Creating the scanner for the user to input the data needed
      
      System.out.print("Enter the cost of the check: "); //Asking for the cost of the check
      double CheckCost = myScanner.nextDouble();
   
      System.out.print("Enter the tip percentage as a whole number: ");//Asking for the tip percentage they want to add
      double tipPercent = myScanner.nextDouble();
      tipPercent /= 100; //dividing by 100 to change to a decimal
    
      System.out.print("Enter the number of people to split the check by: ");//Asking the user how many ways to split the tip
      int numPeople = myScanner.nextInt();
      
      double Total;
      double CostPerPerson;
      
      int Dollars;
      int Dimes;
      int Pennies;
      //Creating a way to store a number for the numbers to the right of the decimal point
      Total = CheckCost * (1 + tipPercent); //Calculating the total cost with tip
      CostPerPerson = Total / numPeople; //Dividing the cost so each person pays the same amount
      Dollars = (int) CostPerPerson; //Dropping the decimal from the cost per person to just get the dollars
      Dimes = (int) (CostPerPerson * 10) % 10; //Finding the number for the tenths place in the decimal
      Pennies = (int) (CostPerPerson * 100) % 10; //Finding the number for the hundreths place in the decimal
      
      System.out.println("Each person has to pay $" + Dollars + "." + Dimes + Pennies); // Printing the price that everyone needs to pay 
    }
}
//Martin Magazzolo
//9/25/18
//CSE02
//This program will simulate a game of Craps using if statements
import java.util.Scanner;
public class CrapsIf {
    public static void main(String[] args) {
      
      Scanner myScanner = new Scanner (System.in); //Creating a scanner to ask if the user wants to pick nummbers or generate random numbers
      
      System.out.print("For random numbers type 'Random'. To input numbers type 'Input': ");
      String Response = myScanner.next(); 
       if (Response.equals("Random")){
         int Dice1 = ((int)(Math.random() * 5) + 1); //generating the first random number from 1 to 6
         int Dice2 = ((int)(Math.random() * 5) + 1); //generating the second random number from 1 to 6
         System.out.println(Dice1 + " " + Dice2); //Printing the two generated numbers
         int total = Dice1 + Dice2; //Finding the sum of the two die
         if(total ==2){ 
             System.out.println("Snake Eyes"); //The only time the total is 2 is with two ones
           }
         else if(total == 3){
             System.out.println("Ace Duece"); //The only time the total is 3 is with a one and a two
         }
         else if(total == 4){
            if (Dice1 == Dice2){
              System.out.println("Hard Four");// since the total is a four and both numbers are the same, its a hard four
            }
            else {
              System.out.println("Easy Four"); //The only other possibility for a four is an easy four
            }
         }
         else if(total == 5){
           System.out.println("Fever Five"); //The only name for a five is a fever five
         }
         else if(total == 6){
           if (Dice1 == Dice2){
             System.out.println("Hard Six"); //If the total is six and both dice are the same, its a hard six
           }
           else{
             System.out.println("Easy Six"); //The other option for a six is an easy six
           }
         }
         else if(total == 7){
           System.out.println("Seven Out"); //The only name for a total of seven is a seven out
         }
         else if(total == 8){
            if (Dice1 == Dice2){
             System.out.println("Hard Eight"); //If the total is Eight and both dice are the same, its a hard eight
           }
           else{
             System.out.println("Easy Eight"); //The only other option for a total of eight is easy eight
           }
         }
         else if(total == 9){
           System.out.println("Nine"); //The only name for a total of nine is nine
         }
         else if(total == 10){
            if (Dice1 == Dice2){
             System.out.println("Hard Ten"); //If the total is Ten and both dice are the same, its a hard Ten
           }
           else{
             System.out.println("Easy Ten"); //The only other option for a total of Ten is easy Ten
           }
         }
         else if(total == 11){
           System.out.println("Yo-leven"); //The only name for a total of eleven is yo-leven
         }
         else if(total == 12){
           System.out.println("Boxcars"); //The name for getting to sixes is boxcars, which is the only way to get a 12
         }
       }
       else if (Response.equals("Input")){
         System.out.println("Input");
         Scanner myScanner2 = new Scanner (System.in);
         System.out.println("Enter number 1 (From 1 to 6): ");
         int Dice1 = myScanner.nextInt();
         System.out.println("Enter number 2 (From 1 to 6): ");
         int Dice2 = myScanner.nextInt();
         //Letting the user input the two numbers to be used for the game
         if(Dice1 > 0 && Dice1 < 7 && Dice2 > 0 && Dice2 < 7){
         int total = Dice1 + Dice2; //Finding the sum of the two die
         if(total ==2){ 
             System.out.println("Snake Eyes"); //The only time the total is 2 is with two ones
           }
         else if(total == 3){
             System.out.println("Ace Duece"); //The only time the total is 3 is with a one and a two
         }
         else if(total == 4){
            if (Dice1 == Dice2){
              System.out.println("Hard Four");// since the total is a four and both numbers are the same, its a hard four
            }
            else {
              System.out.println("Easy Four"); //The only other possibility for a four is an easy four
            }
         }
         else if(total == 5){
           System.out.println("Fever Five"); //The only name for a five is a fever five
         }
         else if(total == 6){
           if (Dice1 == Dice2){
             System.out.println("Hard Six"); //If the total is six and both dice are the same, its a hard six
           }
           else{
             System.out.println("Easy Six"); //The other option for a six is an easy six
           }
         }
         else if(total == 7){
           System.out.println("Seven Out"); //The only name for a total of seven is a seven out
         }
         else if(total == 8){
            if (Dice1 == Dice2){
             System.out.println("Hard Eight"); //If the total is Eight and both dice are the same, its a hard eight
           }
           else{
             System.out.println("Easy Eight"); //The only other option for a total of eight is easy eight
           }
         }
         else if(total == 9){
           System.out.println("Nine"); //The only name for a total of nine is nine
         }
         else if(total == 10){
            if (Dice1 == Dice2){
             System.out.println("Hard Ten"); //If the total is Ten and both dice are the same, its a hard Ten
           }
           else{
             System.out.println("Easy Ten"); //The only other option for a total of Ten is easy Ten
           }
         }
         else if(total == 11){
           System.out.println("Yo-leven"); //The only name for a total of eleven is yo-leven
         }
         else if(total == 12){
           System.out.println("Boxcars"); //The name for getting to sixes is boxcars, which is the only way to get a 12
         }
         }
         else {
           System.out.println("An Invalid number was given");
         }
    }
}
}

                        
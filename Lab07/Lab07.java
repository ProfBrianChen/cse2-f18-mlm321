//Martin Magazzolo
//10/25/18
//CSE02
//This program will generate random gramatically correct sentences
import java.util.Random;
import java.util.Scanner;
public class Lab07 {
    public static String adjectives() {
      Random randomGenerator = new Random();
      int random = randomGenerator.nextInt(10); //generating a random number
      String adjective = "";
     switch (random) {
       case 0: 
       adjective = "aggressive";
       break;
       case 1:
       adjective = "immense";
       break;
       case 2:
       adjective = "scruffy";
       break;
       case 3:
       adjective = "polite";
       break;  
       case 4:
       adjective = "lazy";
       break;
       case 5:
       adjective = "embarassed";
       break;
       case 6:
       adjective = "itchy";
       break;
       case 7:
       adjective = "dazzling";
       break;  
       case 8:
       adjective = "thankful";
       break;
       case 9:
       adjective = "repulsive";
       break;
     }
      return adjective;
    }
  public static String NounSubject () {
      Random randomGenerator = new Random();
      int random = randomGenerator.nextInt(10); //generating a random number
      String NounSubject = "";
     switch (random) {
       case 0: 
       NounSubject = "dog";
       break;
       case 1:
       NounSubject = "ladybug";
       break;
       case 2:
       NounSubject = "lion";
       break;
       case 3:
       NounSubject = "giraffe";
       break;  
       case 4:
       NounSubject = "armadillo";
       break;
       case 5:
       NounSubject = "kangaroo";
       break;
       case 6:
       NounSubject = "gorilla";
       break;
       case 7:
       NounSubject = "cat";
       break;  
       case 8:
       NounSubject = "hamster";
       break;
       case 9:
       NounSubject = "squirrel";
       break;
     }
      return NounSubject;
    }
  public static String Verbs () {
      Random randomGenerator = new Random();
      int random = randomGenerator.nextInt(10); //generating a random number
      String verb = "";
     switch (random) {
       case 0: 
       verb = "danced";
       break;
       case 1:
       verb = "threw";
       break;
       case 2:
       verb = "congratulated";
       break;
       case 3:
       verb = "typed";
       break;  
       case 4:
       verb = "drove";
       break;
       case 5:
       verb = "asked";
       break;
       case 6:
       verb = "watched";
       break;
       case 7:
       verb = "trusted";
       break;  
       case 8:
       verb = "ate";
       break;
       case 9:
       verb = "risked";
       break;
     }
      return verb;
    }
  public static String NounObject() {
      Random randomGenerator = new Random();
      int random = randomGenerator.nextInt(10); //generating a random number
      String NounObject = "";
     switch (random) {
       case 0: 
       NounObject = "man";
       break;
       case 1:
       NounObject = "town";
       break;
       case 2:
       NounObject = "sandbox";
       break;
       case 3:
       NounObject = "computer";
       break;  
       case 4:
       NounObject = "elevator";
       break;
       case 5:
       NounObject = "telephone";
       break;
       case 6:
       NounObject = "bathroom";
       break;
       case 7:
       NounObject = "table";
       break;  
       case 8:
       NounObject = "backpack";
       break;
       case 9:
       NounObject = "concert";
       break;
     }
      return NounObject;
    }
    public static String Sentence(){
      String subject = adjectives();
      String Sentence = "The "+ subject + " " + adjectives() + " " + NounSubject() + " " + Verbs() + " the " + adjectives() + " " + NounObject() + "."; //Creating a sentence with each random word
      return Sentence; //returning the sentence to the main method
    }
    public static String Conclusion(){
      String Conclusion = "They " + Verbs() + " their " + NounSubject();
      return Conclusion;
    }
    public static void main(String[] args) {
         Scanner User = new Scanner (System.in);
         System.out.println("This is your thesis sentences: " + Sentence()); //showing the first sentence
         System.out.println("Type 1 for another sentence, 2 for no sentence."); //ASkinh the user if they want to continue the paragraph
         int x = User.nextInt(); //Taking the users input
      while (x == 1){ //Whenever the user chooses 1, they are given a sentence and asked again if they want another sentence
        System.out.println(Sentence());//Another sentence is generated and printed
        System.out.println("Type 1 for another sentence, 2 for no sentence.");
        x = User.nextInt();
      }
      System.out.println("Type 1 if you want to finish your paragraph with a conclusion sentence.");
      x = User.nextInt();
      if (x==1){
        System.out.println(Conclusion());
      }
     }
    }
         
         
         
         
         
         
         
         
         
         
         
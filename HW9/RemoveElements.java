//Martin Magazzolo
//11/27/18
//CSE02
//This program will arrange and search for final grades
import java.util.Scanner;
import java.util.Random;
public class RemoveElements{
  public static int[] randomInput(){
    Random randomGenerator = new Random();
    int array[] = new int[10]; // creating a new array of size 10
    for (int i = 0; i <10; i++){ //looping from element 1 to element 10
      array[i] = randomGenerator.nextInt(10); //assigning each element to a random number from 1 to 10
    }
    return array; //returning the array
    
  }
  public static int[] delete(int[] number, int index){
    int size = number.length; //finding the size of the array
    int deletedArray[] = new int[size-1]; //making an array to store the new array 
    for(int y = 0; y < index; y++){ //looping from the first element to right before the element to be deleted
      deletedArray[y] = number[y]; //copying each element before the deleted one to the new array
    }
    for (int x = index; x < (size-1); x++){ //loopin from the deleted element to the last element
      deletedArray[x] = number[x+1]; //assigning the spot in the new array to the next element in the first array
    }
    return deletedArray; //returning the new array
  }
  public static int[] remove(int[] list, int target){
    int size = list.length; //finding the size of the array
    for (int x =0; x<size; x++){ //looping from the first to the last element
      if (list[x] == target) //determining if each element is the targeted number
        list = delete(list, x); //using the delete method to create a new array without the targeted number
        size--; //since the array is one smaller now, the size must be decreased so it doesnt loop past the size of the array
    }
    return list; //returning the new array
  }
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
    while(index < 0 || index > 10){ // a while loop that will give an error when there is an invalid input
      System.out.println("The index is not valid.");
      index = scan.nextInt();
    }
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
}
 

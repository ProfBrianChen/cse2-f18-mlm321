//Martin Magazzolo
//11/27/18
//CSE02
//This program will arrange and search for final grades
import java.util.Scanner;
import java.util.Random;
public class CSE2Linear {
  public static String Binary(int array[]){
    Scanner myScanner = new Scanner (System.in);
    int itterations = 0; //setting a counter for the amount of itterations
    int low = 0; //setting a variable for the bottom of the range of numbers to be checked
    int high = 14;//setting a variable for the top of the range of numbers to be checked
    System.out.println("Enter a grade to be searched for.");
    int input = myScanner.nextInt(); //storing the inputted value
    while (high>=low){ //a while loop that will loop as long as there are still numbers to be checked
    int mid = ((high + low) / 2); //setting the middle number as the average of the high and low
    if(input == array[mid]){ //if statement to determine if the middle number is the desired number
     itterations++; //increasing the counter
     String output = ("The grade was found in " + itterations + " itterations."); 
     return output; //returning the output
    }
    else if(input > array[mid]){
      low = mid + 1; //if the input is higher than the middle number, then the bottom half can be ignored, so the new low is one higher than the previous middle
      itterations++; //increasing the counter
    }
    else if(input < array[mid]){
      high = mid - 1; //if the input is lower than the middle number, then the top half can be ignored, so the new high is one lower than the previous middle
      itterations++; //increasing the counter
    }
  }
     String output = ("The grade was not found in " + itterations + " itterations."); 
     return output; //if there are no more numbers to check and the number was not found, this output is returned
  }
  public static String Linear(int array[]){
    Scanner myScanner = new Scanner (System.in);    
    System.out.println("Enter a grade to be searched for.");
    int input = myScanner.nextInt(); 
    for (int i = 0; i < 15; i++){ //creating a loop for each element of the array
      if (array[i] == input){ //checking if the element of the array is equal to the desired number
        String output = "The grade was found in " + (i+1) + " itterations"; //the number of itterations is correlated to the amount of times the loop has been gone through
        return output; //this output is returned if the number was found
      }
    }
    String output = "The grade was not found in 15 itterations";
    return output; //this output is returned if the number was not found
  }
  public static int[] scrambling(int input[]){
    for(int x =0; x<1000; x++){ //looping 1000 times so it is definitely shuffled
      Random randomGenerator =  new Random(); //creating a random number generator
      int Random = randomGenerator.nextInt(15); //generating a number from 1 to 15
      int old = input[0]; //storing the first number to swap it
      input[0] = input[Random]; //reassigning the first spot to the number from the random spot
      input[Random] = old; //Putting the old first number into the random spot
    }
  return input; //returning the array
  }
  public static void main(String [] args){
    Scanner myScanner = new Scanner (System.in);
    int[] grades = new int[15]; //Declaring an array of size 15 to hold the final grades
    System.out.println("Enter 15 numbers for the final grades in ascending order.");
      for(int x = 0; x < 15; x++){//Creating a for loop that loops 15 times for each number        
         if(myScanner.hasNextInt()== true) {//Verifying if the inputted number is an integer
             int temporary = myScanner.nextInt(); //assigning the integer to a temporary variable
              if(temporary >= 0 && temporary <= 100){ //if the number inputted is withing the correct range, this will be true
                if ( x == 0 || grades[x-1] <= temporary){ //This will be true as long as the new number is higher than the last number, or when x=0 since that is the first number inputted
                    grades[x] = temporary; //if all checks have been cleared, the number is added to the array.
                   }
                  else{
                    System.out.println("Error. The number inputted was smaller than the last number"); //if the number was not in increasing order, an error is shown
                    x--; //decreasing the counter because this inputted number was not valid
                   }
              }
              else{
                 System.out.println("Error. The number inputted was outside the range 0-100."); //if the number is not within the range,an error is shown
                 x--;//decreasing the counter because the inputted number was not valid
              }  
         }
        else{
          System.out.println("Error. The input must be an integer."); //if the input is not an integer, an error is shown
          myScanner.next(); //Reseting the scanner
          x--;//decreasing the counter.
        }
    }
     for(int y = 0; y < 15; y++){ //printing the array
       System.out.print(grades[y] + " ");
      }
  System.out.println();
  String output = Binary(grades);
  System.out.println(output);
  int[] newArray = scrambling(grades);
  System.out.println("Scrambled:");
    for(int y = 0; y < 15; y++){
       System.out.print(newArray[y] + " ");
      }
  output = Linear(newArray);
  System.out.println(output);

}
}
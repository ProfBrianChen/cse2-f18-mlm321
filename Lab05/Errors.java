//Martin Magazzolo
//10/4/18
//CSE02
//This program will ask the user for inputs about a class, and check to make sure the inputs are valid
import java.util.Scanner;
public class Errors {
    public static void main(String[] args) {
      
      Scanner myScanner = new Scanner (System.in); //Creating a scanner
      int x = 0; //Starting a counter
      
      while (x == 0) { //While the counter is at the original number the loop will run
      System.out.println("What is your course number?"); //Prompting the user to input their course number
        if (myScanner.hasNextInt() == true) { //Determining if the user inputted an integer
          int CourseNumber = myScanner.nextInt(); //Assigning the input to an integer
          x = 1; //Increasing the counter by one
        }
        else {
          System.out.println("Error: Integer input required"); //If an integer wasnt inputted, an error message is displayed
          myScanner.next(); // Resetting the scanner
        }
      }
      
      while (x == 1) {
      System.out.println("What is the department name?");
        if (myScanner.hasNextInt() != true) {
          String Department = myScanner.next();
          x = 2;
        }
        else {
          System.out.println("Error: String input required");
          myScanner.next();
        }
      }
      
      while (x == 2) {
      System.out.println("How many times do you meet per week?");
        if (myScanner.hasNextInt() == true) {
          int TimesPerWeek = myScanner.nextInt();
          x = 3;
        }
        else {
          System.out.println("Error: Integer input required");
          myScanner.next();
        }
      }
      
      while (x == 3) {
      System.out.println("What time does the class start?");
        if (myScanner.hasNextInt() == true) {
          int Time = myScanner.nextInt();
          x = 4;
        }
        else {
          System.out.println("Error: Integer input required");
          myScanner.next();
        }
      }
      while (x == 4) {
      System.out.println("What is your instructor's name?");
        if (myScanner.hasNextInt() != true) {
          String InstructorsName = myScanner.next();
          x = 5;
        }
        else {
          System.out.println("Error: String input required");
          myScanner.next();
        }
      }
      while (x == 5) {
      System.out.println("How many students are in your class?");
        if (myScanner.hasNextInt() == true) {
          int Students = myScanner.nextInt();
          x = 6;
        }
        else {
          System.out.println("Error: Integer input required");
          myScanner.next();
        }
      }
    }
}

       
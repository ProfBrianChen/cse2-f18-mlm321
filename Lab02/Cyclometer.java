//Martin Magazzolo
//9/6/18
//CSE02
//This program is creating a cyclometer that records the time, rotation count, and distance of a trip.
public class Cyclometer {
    
   	public static void main(String[] args) {
      int secsTrip1=480;  // The time elapsed in seconds of the first trip
      int secsTrip2=3220;  //The time elapsed in seconds of the second trip
		  int countsTrip1=1561;  //The amount of tire rotations during the first trip
		  int countsTrip2=9037; //The amount of tire rotations during the second trip
      double wheelDiameter = 27.0; // Helpful data for calculations
  	  double PI = 3.14159; // Helpful data for calculations
  	  int feetPerMile = 5280;  //Helpful conversion for calculations
  	  int inchesPerFoot = 12;   //Helpful conversion for calculations
  	  int secondsPerMinute = 60;  //Helpful conversion for calculations 
	    double distanceTrip1,distanceTrip2,totalDistance;  //Creating variables to store the calculated numbers for the distances travelled
        System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+" minutes and had "+ countsTrip1+" counts.");
	      System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+ countsTrip2+" counts.");
        // This is printing the data that was given to us for the time of each trip and the counts for each trip
        // The time is given in minutes
      distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Multiplying the counts by the diameter of the wheel and pi to give the distance of trip 1
	    distanceTrip1/=inchesPerFoot*feetPerMile; // Converts the distance to miles
	    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; 
      //Finding the distance of trip 2 and concerting it to miles in one step
	    totalDistance=distanceTrip1+distanceTrip2;// Adding the two distances to find the total distance travelled
      System.out.println("Trip 1 was "+distanceTrip1+" miles");
	    System.out.println("Trip 2 was "+distanceTrip2+" miles");
	    System.out.println("The total distance was "+totalDistance+" miles");
      //Printing out the final calculated data
	}    
} 

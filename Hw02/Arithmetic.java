//Martin Magazzolo
//9/11/18
//CSE02
//This program is going to calculate the price of a shopping trip
public class Arithmetic {
    
   	public static void main(String[] args) {
      int Pants = 3; //The number of pants bought
      double PantsPrice = 34.98; //The price of each pair of pants
     
      int Sweatshirts = 2; //The number of sweatshirts bought
      double SweatshirtsPrice = 24.99; //The price of each sweatshirt
      
      int Belts = 1; //The number of belts bought
      double BeltsPrice = 33.99; //The price of each belt
      
      double SalesTax = 0.06; //The tax rate for PA
      
      double TotalPants = (int) Pants * (double) PantsPrice; //Calculating the total money spent on pants
      double TotalSweatshirts = (int) Sweatshirts * (double) SweatshirtsPrice; //Calculating the total money spent on sweatshirt
      double TotalBelts = (int) Belts * (double) BeltsPrice; //Calculating the total money spent on belts
      
      double PantsTax = (double) TotalPants * (double) SalesTax; //Finding the price of tax for the pants
      double SweatshirtsTax = (double) TotalSweatshirts * (double) SalesTax; //Finding the price of tax for the sweatshirts
      double BeltsTax = (double) TotalBelts * (double) SalesTax; //Finding the price of tax for the belts
      
      double Total = (double) TotalBelts + (double) TotalSweatshirts + (double) TotalPants; //Calculating the total money spend before tax
      Total = Total * 100;
      double TotalFixed = (int) Total;
      TotalFixed = TotalFixed / 100;
      //Truncating the value for the Total spent before tax
      
      double TotalTax = (double) PantsTax + (double) SweatshirtsTax + (double) BeltsTax; //Calculating the total price of tax
      TotalTax = TotalTax * 100;
      double TotalTaxFixed = (int) TotalTax;
      TotalTaxFixed = TotalTaxFixed / 100;
      //Truncating the value for the Total spent on tax
      
      double OverallTotal = (double) TotalFixed + (double) TotalTaxFixed; //Calculating the overall total money spent
      OverallTotal = OverallTotal * 100;
      double OverallTotalFixed = (int) OverallTotal;
      OverallTotalFixed = OverallTotalFixed / 100;
      //Truncating the value for the Total money spent
      
      System.out.println("Price before tax:" + TotalFixed); //Displaying the price before tax
      System.out.println("Price of tax:" + TotalTaxFixed); //Displaying the price of tax
      System.out.println("Total Price:" + OverallTotalFixed); //Displaying the overall total money spent
    }
}
      
      
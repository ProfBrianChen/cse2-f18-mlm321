//Martin Magazzolo
//9/20/18
//CSE02
//This program is going to randomly generate a card from a deck
public class CardGenerator{
    public static void main(String[] args) {
      
      int Card = (int)(Math.random() * 51); // Generating a number from 0 to 1 then scaling it to a number between 0 and 51
      
      String suit = "0"; 
      String number = "0";
      //Creating string variables to assign the cards number and suit
      
      if (0 <= Card && Card <= 12) {
        suit = "Diamonds";
      }
      else if (13 <= Card && Card <= 25) {
        suit = "Clubs";
      }
      else if (26 <= Card && Card<= 38) {
        suit = "Hearts";
      }
      else if (39 <= Card && Card <= 51){
        suit = "Spades";
      }
      // Using if statements to assign the cards suit to the string for it's suit.  Each range of numbers is a different suit
      
      int remainder = Card % 13; //Creating groups of 13 to be able to assign the card number based on the remainder
      switch( remainder) {
        case 0:
          number = "Ace";
        break;
        case 1:
          number = "Two";
        break;
        case 2:
          number = "Three";
        break;
        case 3:
          number = "Four";
        break;
        case 4:
          number = "Five";
        break;
        case 5:
          number = "Six";
        break;
        case 6:
          number = "Seven";
        break;
        case 7:
          number = "Eight";
        break;
        case 8:
          number = "Nine";
        break;
        case 9:
          number = "Ten";
        break;
        case 10:
          number = "Jack";
        break;
        case 11:
          number = "Queen";
        break;
        case 12:
          number = "King";
        break;
      }
      //Using the remainder to assign a number to our card based on what the remainder is.  Each suit will have one number that has each remainder in this order so the card numbers will be the same for each suit
     
      System.out.println ("Your Card is the " + number + " of " + suit);
    }
}
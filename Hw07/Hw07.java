//Martin Magazzolo
//10/30/18
//CSE02
//This program will analyze a user inputted text sample
import java.util.Scanner;
public class Hw07 {
    public static String sampleText(String input){
      String text = input;
      System.out.println("You've entered:"); //Displaying the text sample that the user inputted
      System.out.println(text);
      return text; //returning the string back to the main method
    }
    public static String printMenu(){
      Scanner menuScanner = new Scanner (System.in); //creating a new scanner for the user to input their choice
      //Creating the list of options the user can choose
      System.out.println("Menu:");
      System.out.println("c: Number of non-whitespace characters");
      System.out.println("w: Number of words");
      System.out.println("f: Find Text");
      System.out.println("r: Replace all !'s");
      System.out.println("s: Shorten spaces");
      System.out.println("q: Quit");
      System.out.println("Choose one of these options");
      String choice = menuScanner.nextLine(); 
      return choice;//returning the users choice back to the main method
    }
    public static int getNumOfNonWSCharacters(String input){
      int length = input.length(); //Finding how many total characters there are
      int y = 0; //a counter for the amount of non ws characters
      for (int x = 0;x <length; x++){ //Creating a loop to check every character
        if (input.charAt(x) != ' '){ //if the character at the given spot is not a space, the counter for characters is increased by one
          y++;
        }
      }
      return y; //Returning the number of character back to the main method
    }
    public static int getNumOfWords(String input){
      int length = input.length(); //Finding how many total characters there are
      int y = 1; //a counter for the amount of words starting at one to count the first word
      for (int x = 0;x <length; x++){ //Creating a loop that will check every character
        if (input.charAt(x) == ' ' && input.charAt(x+1) != ' '){ //if the character is a space, and the next character is not a space, then that means a new wordhas been started
          y++; //Since a new word has been started, the counter for words is increased
        }
      }
      return y; //Returning the value for the amount of words back to the main method
    }
    public static String findText(String input){
      Scanner find = new Scanner (System.in); //Creating a new scanner for the user to input the phrase they want to find
      System.out.println("Enter a word or phrase to be found: ");
      String phrase = find.nextLine(); //Assigning the phrase to a string
      int index = input.indexOf(phrase); //This will output a number of -1 if the word is not found in the string
      int count = 0; //Creating a counter to count the times the phrase is found
       while (index != -1) { //Making a loop that goes every time phrase is found in the inputted string
         count++; //increassing the counter since the phrase was found
         input = input.substring(index + 1); //Creates a new string that does not include all the string before the phrase was found, so there are no recounts
         index = input.indexOf(phrase); //finding the index for the new string
        }
      String result = "Number of times the word " + phrase +" appears: " + count; //Creating a string that contains the phrase and how many times it was found
      return result;// returning the string back to the main mathod
    }
    public static String replaceExclamation(String input){
      String Edited = input.replace("!","."); //This command replaces all the characters before the comma with the character after the comma
      return Edited; //returning the new string back to the main method
    }
    public static String shortenSpace(String input){
      String Edited = input.replaceAll("\\s+"," "); //  \\s is used to encompass whenever there are multiple spaces and replaces with one space
      return Edited;//returning the new string
    }
    public static void main(String[] args) {
      String choice = ""; //declaring the string for the choice the user choses from the menu
      Scanner myScanner = new Scanner (System.in); //Creating a scanner for the user to input a text sample
      System.out.println("Enter a sample text");
      String text = sampleText(myScanner.nextLine());
      while(!choice.equals("q")){ //creating a loop so that the menu reappears after every commany except for the quit command
      choice = printMenu(); //allowing the user to choose a new command
        if(choice.equals("c")) {
          int characters = getNumOfNonWSCharacters(text);
          System.out.println("Number of non-whitespace characters:" + characters);
        }
        if(choice.equals("w")) {
          int words = getNumOfWords(text);
          System.out.println("Number of words:" + words);
        }
        if(choice.equals("f")){
          String instances = findText(text);
          System.out.println(instances);
        }
        if(choice.equals("r")){
          String Edited = replaceExclamation(text);
          System.out.println(Edited);
        }
        if(choice.equals("s")){
          String Edited = shortenSpace(text);
          System.out.println(Edited);
        }
      }
      }
    }
